#!/bin/bash
## This script will install a 64-bit version of Vegas Pro on Linux using Wine

# Variables
## This is useful for if you want to install a different version of 64-bit Vegas Pro. This is where you can easily change things without looking through the script.

# Wine prefix
export WINEPREFIX=~/.vegaspro14

SETUP=$1 # User should input the installer file after the "./vegaspro14linux.sh" command
VEGASVER="VEGAS Pro 14.0" # CASE SENSITIVE! Needs to be exactly as branded! Magix brands Vegas all uppercase like VEGAS. Sony brands it like "Vegas". They should end in ".0"
VEGASEXE="vegas140.exe" # Change acordingly based on version of Vegas Pro
ROOTVEGASFILES="VEGAS" # Should be either "Sony" or "VEGAS". Versions 13 and older are "Sony". Versions 14 and newer are "VEGAS".

# Prevents this script from being run as root

if (( $EUID == 0 )); then
    echo "Please run this script as a normal user."
    echo "Running this script as root is a very bad idea."
    exit 1
fi

# Prevents this script from being run on non-64 bit systems (such as i386 or ARM)

if [ ! $(uname -m) = "x86_64" ]; then
    echo "This script is made for 64-bit systems only."
    echo "This system is not supported."
    exit 1
fi

# Make sure the user has the necessary prerequisites

function require_binary {
if ! [ -x "$(command -v "$1")" ]; then
    echo "The required executable '$1' is not installed."
    exit 1
fi
}

require_binary wine
require_binary winetricks
require_binary cabextract

function require_gnutls_arch {
if [ ! -f "/usr/lib32/libgnutls.so" ]; then
    echo "The required 32-bit binaries for 'gnutls' is not installed."
    exit 1
fi
}

function require_gnutls_deb {
if [ ! -f "/usr/lib/i386-linux-gnu/libgnutls.so.30" ]; then
    echo "The required 32-bit binaries for 'gnutls' is not installed."
    exit 1
fi
}

function require_gnutls_suse {
if [ ! -f "/usr/lib/libgnutls.so.30" ]; then
    echo "The required 32-bit binaries for 'gnutls' is not installed."
    exit 1
fi
}

if [ -x "$(command -v "$pacman")" ]; then
    require_gnutls_arch
fi

if [ -x "$(command -v "$dpkg")" ]; then
    require_gnutls_deb
fi

if [ -x "$(command -v "$zypper")" ]; then
    require_gnutls_suse
fi

if [ ! -f "$SETUP" ]; then
    echo "$VEGASVER installation file not found."
    echo "Please specify the location of the setup file for $VEGASVER."
    exit 1
fi

if [ $SETUP = *.msi ]; then
    SETUPEXEC="msiexec /i"; else
    if [ $SETUP = *.exe ]; then
    SETUPEXEC="wine"; else
    echo "Invalid installation file. Installation file must be either a '.EXE' file or a '.MSI' file." && exit 1; fi
fi

# Creates a log folder for trouble shooting purposes

if [ ! -d "./logs" ]; then
    mkdir "./logs"
fi

# Detect the Wine prefix and ask user if they want to delete the prefix

function prefix_detect_and_ask {
if [ -d "$WINEPREFIX" ]; then
    echo "Wine prefix ($WINEPREFIX) has been detected."
    read -p "Delete this prefix and start fresh? (yes/no/cancel): " choice
    case "$choice" in 
        yes|YES|Yes|y|Y ) echo "Deleting '$WINEPREFIX'..." && rm -rf $WINEPREFIX;;
        no|NO|No|n|N ) echo "Wine prefix will not be deleted.";;
        cancel|CANCEL|Cancel|c|C ) echo "Script aborted." && exit 1 ;; 
        * ) echo "Invalid Answer" && prefix_detect_and_ask;;
    esac
fi
}

prefix_detect_and_ask

# Wine prefix setup

if [ ! -d "$WINEPREFIX" ]; then
    echo "Creating new Wine prefix..."
    WINEDLLOVERRIDES="mscoree=d;mshtml=d" wineboot -u > ./logs/1-prefix-create.log 2>&1
    echo "Created $WINEPREFIX"
fi

# Microsoft .NET Framework 3.5 and 4 installation and setup process

echo "Installing .NET 3.5 and 4. Go through installation wizards."
echo "Select 'Restart Later' when finished installing .NET 4."
winetricks "dotnet35" "dotnet40" > ./logs/2-dotnet.log 2>&1


# Installing Vegas Pro

echo "Installing $VEGASVER..."
$SETUPEXEC $SETUP > ./logs/3-vegas-setup.log 2>&1

# For whenever the installer fails to do what it's supposed to do or if the user cancels the installer

if [ ! -f "$WINEPREFIX/drive_c/Program Files/$ROOTVEGASFILES/$VEGASVER/$VEGASEXE" ]; then
    echo "Installation failed or aborted."
    echo "Check the log files for any errors."
    exit 1
fi

# Post installation fixes

rm "$WINEPREFIX/drive_c/Program Files/$ROOTVEGASFILES/$VEGASVER/FileIOProxyStubx64.dll"
rm -r "$WINEPREFIX/drive_c/Program Files/$ROOTVEGASFILES/$VEGASVER/FileIO Plug-Ins/compoundplug"

# Display message of completion

echo "Installation of $VEGASVER complete!"
